import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { credentials, token } from '@core/models/auth';
import { ApiService } from '@core/services/api/api.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  form: FormGroup;
  isError: boolean = false;
  errorMsg: string;

  constructor(private readonly fb: FormBuilder,
    private readonly _api: ApiService,
    private readonly _router: Router) {

  }

  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    const credentials: credentials = this.form.value
    this._api.login(credentials).subscribe(
      (data: token) => {
        localStorage.setItem('token', data.token);
        this._router.navigate(['movies']);
      },
      (error) => {
        this.isError = true;
        this.errorMsg = error.error.err;

        // hide the error msg after 2.5 secondes
        setTimeout(() => {
          this.isError = false;
        }, 2500);
      }
    )
  }
}
