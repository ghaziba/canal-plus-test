import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private readonly _router: Router) {

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const isAuthenticated = localStorage.getItem("token") ? true : false;
    const data = route.data;

    if (data['data'] === 'movies') {
      if (isAuthenticated)
        return true;
      else
        this._router.navigate(['/auth']);
      return false
    } else {
      if (isAuthenticated) {
        this._router.navigate(['/movies'])
        return false;
      } else
        return true;
    }
  }

}
