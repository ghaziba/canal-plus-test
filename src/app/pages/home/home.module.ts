import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesComponent } from './movies/movies.component';
import { HomeRoutingModule } from './home-routing.module';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { SharedModule } from '@shared/shared.module';
import { HomeComponent } from './home.component';



@NgModule({
  declarations: [
    MoviesComponent,
    MovieDetailsComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ]
})
export class HomeModule { }
