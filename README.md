## Install dependencies

Run `npm install`
## Development server

Run `ng serve` or `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Login to the app

To connect with canal-plus user you need to put this credentials:
`username: "Canal-plus"` 
`password: "Super-secret"`

## Documentation

Run `npm run doc:build` to generate the app documentation folder

Run `npm run doc:serve` to serve the documentation. Navigate to `http://localhost:8080/`

![IMAGE_DESCRIPTION](https://gitlab.com/ghaziba/canal-plus-test/-/raw/main/src/assets/screen-shots/doc-canal-plus.png)

## screenshots
![IMAGE_DESCRIPTION](https://gitlab.com/ghaziba/canal-plus-test/-/raw/main/src/assets/screen-shots/login.png)
![IMAGE_DESCRIPTION](https://gitlab.com/ghaziba/canal-plus-test/-/raw/main/src/assets/screen-shots/movies.png)
![IMAGE_DESCRIPTION](https://gitlab.com/ghaziba/canal-plus-test/-/raw/main/src/assets/screen-shots/search.png)
![IMAGE_DESCRIPTION](https://gitlab.com/ghaziba/canal-plus-test/-/raw/main/src/assets/screen-shots/details.png)


