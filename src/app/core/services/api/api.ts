import { environment } from "src/environments/environment";
/**
 * API to allow operators to manage foreign exchanges
 *
 * @readonly
 * @class API
 */

export class API {

    static apiPath = environment.apiHost;

    /**
   * @description Core path
   */
  static get core(): string {
    return API.apiPath ;
  }

    static get auth(): string {
        return 'auth';
    }

    static get login(): string {
        return 'login';
    }

    static get movies(): string {
        return 'movies';
    }

    static get movie(): string {
        return 'movie';
    }
}
