import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ApiService } from '@core/services/api/api.service';


@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent {

  movieKeys = ['Title', 'US Gross', 'US DVD Sales', 'Worldwide Gross', 'Production Budget', 'Release Date', 'Distributor', 'IMDB Rating', 'IMDB Votes', 'Major Genre', 'Director', 'Rotten Tomatoes Rating'];
  searchQuery: string;
  sortByKey : string | number;
  constructor(
    private readonly _router: Router,
    private readonly _api: ApiService
  ) { }

  navigateToMoviesWithFilters() {
    const queryParams: NavigationExtras = { queryParams: { query: this.searchQuery, sortBy:this.sortByKey } }
    this._router.navigate(
      ['/movies'],
      queryParams
    );
  }

  logout() {
    this._api.logout();
  }
}
