export interface credentials {
    username: string;
    password: string;
}

export interface token {
    token: string;
}