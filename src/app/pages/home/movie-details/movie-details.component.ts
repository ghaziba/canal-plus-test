import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Movie } from '@core/models/movie';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '@core/services/api/api.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit, OnDestroy{

  movie$ : Observable <Movie>;

  private _routeParams$: Subscription;

  constructor(
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _api: ApiService
    ) { }

    ngOnInit() {
      this._routeParams$ = this._activatedRoute.params.subscribe(
        (params) => {
          this._getMovieById(params.id);
        }
      );
    }

    ngOnDestroy(): void {
      this._routeParams$.unsubscribe();
    }

    private _getMovieById (id: string) {
      this.movie$ = this._api.getMovieById(id);
    }

}
