import { Component, OnDestroy, OnInit } from '@angular/core';
import { Movie } from '@core/models/movie';
import { ApiService } from '@core/services/api/api.service';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit, OnDestroy  {

  movies$ : Observable <Movie []>;
  searchQuery :string ;
  private _routeParams$: Subscription;

  constructor(
    private readonly _api: ApiService,
    private readonly _activatedRoute: ActivatedRoute
    ) { }

  ngOnInit() {
    this._routeParams$ = this._activatedRoute.queryParams.subscribe(
      (params) => {
        this.searchQuery = params.query;
        if (Object.keys(params).length === 0) {
          this.movies$ = this._api.getMovies();
        } else {
          this.movies$ = this._api.findMovie(params);
        }
      }
    );
  }

  ngOnDestroy(): void {
    this._routeParams$.unsubscribe();
  }

}
