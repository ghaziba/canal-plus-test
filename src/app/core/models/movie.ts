export interface Movie {
    id: number;
    Title: string;
    US_Gross?: number;
    US_DVD_Sales?: number;
    Worldwide_Gross?: number;
    Production_Budget?: number;
    Release_Date?: string;
    Distributor?: string;
    IMDB_Rating?: number;
    IMDB_Votes?: number;
    Major_Genre?: string;
    Director?: string;
    Rotten_Tomatoes_Rating?: string;
}