import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `<app-search-bar></app-search-bar>
             <router-outlet></router-outlet>`,
})
export class HomeComponent {

}
