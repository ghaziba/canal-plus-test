import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Movie } from '../../models/movie';
import { Observable } from 'rxjs';

import { API } from '@core/services/api/api';
import { credentials, token } from '@core/models/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private readonly _http: HttpClient,
    private readonly _router: Router) { }


  // --------------------- Authentication ------------

  login(credentials: credentials): Observable<token> {
    const url = `${API.core}/${API.auth}/${API.login}`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const body = JSON.stringify(credentials);
    return this._http.post<token>(url, body, options);
  }

  // ---------------------  movies ------------------

  /**
   * Make Http request to get movies list 
   * @description get all the movies
   * @returns  {HttpResponse} movies array as Observable
   */

  getMovies(): Observable<Movie[]> {
    const url = `${API.core}/${API.movies}`;
    return this._http.get<Movie[]>(url);
  }

  /**
   * Make Http request to search for a movie
   * @description get movie by suggestion name
   * @param {string} query - search key , it should be a portion of title
   * @param {string} sortBy - sort key , it could be  (Title, Director ...)
   * @returns  {HttpResponse} movies array as Observable
   */

  findMovie(query: any): Observable<Movie[]> {
    const url = `${API.core}/${API.movies}`;
    let params = new HttpParams();
    if (query.query)
      params = params.set('query', query.query);
    if (query.sortBy && query.sortBy !== '')
      params = params.set('sortBy', query.sortBy);
    return this._http.get<Movie[]>(url, { params });
  }

  /**
   * Make Http request to get a movie by id
   * @description get a movie by id
   * @param {string} id - movie id
   * @returns  {HttpResponse} movie object as Observable
   */

  getMovieById(id: string): Observable<Movie> {
    const url = `${API.core}/${API.movies}/${id}`;
    return this._http.get<Movie>(url);
  }

  /**
   * Logout from the app
   * @description remove token from localStorage & redirect the user to auth page
   */
  logout() {
    localStorage.removeItem('token');
    this._router.navigate(['auth'])
  }
}
