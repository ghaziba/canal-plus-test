import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '@core/services/api/api.service';
import { Observable,tap } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(
    private readonly _api: ApiService,
    ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = localStorage.getItem("token");

    if (token) {
      const cloned = req.clone({
        headers: req.headers.set("Authorization",
          "Bearer " + token)
      });
      return next.handle(cloned).pipe(
        tap(() => {
          },
          (error) => {
            if (error instanceof HttpErrorResponse && (error.status === 403 || error.status === 401)) {
              //disconnect the user when http error with (401 or 403)status code occured 
              this._api.logout();
            }
          })
      );
    }
    else {
      return next.handle(req);
    }

  }
}
