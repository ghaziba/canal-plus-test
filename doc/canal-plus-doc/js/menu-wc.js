'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">canal-plus-test documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-569e258ac5c1b71d4a535f5aec1da79db24f1f4588658e20032092d54fb0c221deeeae1ad3feb5605653ec301224203dc02316da001a0bc6283b0c416e29c7af"' : 'data-target="#xs-components-links-module-AppModule-569e258ac5c1b71d4a535f5aec1da79db24f1f4588658e20032092d54fb0c221deeeae1ad3feb5605653ec301224203dc02316da001a0bc6283b0c416e29c7af"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-569e258ac5c1b71d4a535f5aec1da79db24f1f4588658e20032092d54fb0c221deeeae1ad3feb5605653ec301224203dc02316da001a0bc6283b0c416e29c7af"' :
                                            'id="xs-components-links-module-AppModule-569e258ac5c1b71d4a535f5aec1da79db24f1f4588658e20032092d54fb0c221deeeae1ad3feb5605653ec301224203dc02316da001a0bc6283b0c416e29c7af"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link" >AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AuthModule-683ed7d9bec413d80d3516e2e34fbb465bdfbcaaf1b44841ec807ae961fd594f54cdd5e67b75b98fee95e023b47a861afb2a9dbf5e015727d9cbf1c7790aaae8"' : 'data-target="#xs-components-links-module-AuthModule-683ed7d9bec413d80d3516e2e34fbb465bdfbcaaf1b44841ec807ae961fd594f54cdd5e67b75b98fee95e023b47a861afb2a9dbf5e015727d9cbf1c7790aaae8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AuthModule-683ed7d9bec413d80d3516e2e34fbb465bdfbcaaf1b44841ec807ae961fd594f54cdd5e67b75b98fee95e023b47a861afb2a9dbf5e015727d9cbf1c7790aaae8"' :
                                            'id="xs-components-links-module-AuthModule-683ed7d9bec413d80d3516e2e34fbb465bdfbcaaf1b44841ec807ae961fd594f54cdd5e67b75b98fee95e023b47a861afb2a9dbf5e015727d9cbf1c7790aaae8"' }>
                                            <li class="link">
                                                <a href="components/SigninComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SigninComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AuthRoutingModule.html" data-type="entity-link" >AuthRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link" >CoreModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/HomeModule.html" data-type="entity-link" >HomeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomeModule-e8acabd35f97ba3ac1de7373f37b0e35d780849c14617866d2f1ecd24637b5432e384bc918380a6eb644541dc86c7807fc21db1abc075f0bc404ee4c1fb93021"' : 'data-target="#xs-components-links-module-HomeModule-e8acabd35f97ba3ac1de7373f37b0e35d780849c14617866d2f1ecd24637b5432e384bc918380a6eb644541dc86c7807fc21db1abc075f0bc404ee4c1fb93021"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomeModule-e8acabd35f97ba3ac1de7373f37b0e35d780849c14617866d2f1ecd24637b5432e384bc918380a6eb644541dc86c7807fc21db1abc075f0bc404ee4c1fb93021"' :
                                            'id="xs-components-links-module-HomeModule-e8acabd35f97ba3ac1de7373f37b0e35d780849c14617866d2f1ecd24637b5432e384bc918380a6eb644541dc86c7807fc21db1abc075f0bc404ee4c1fb93021"' }>
                                            <li class="link">
                                                <a href="components/HomeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MovieDetailsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MovieDetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MoviesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MoviesComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeRoutingModule.html" data-type="entity-link" >HomeRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' : 'data-target="#xs-components-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' :
                                            'id="xs-components-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' }>
                                            <li class="link">
                                                <a href="components/SearchBarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SearchBarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' : 'data-target="#xs-directives-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' :
                                        'id="xs-directives-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' }>
                                        <li class="link">
                                            <a href="directives/HighlightDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HighlightDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' : 'data-target="#xs-pipes-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' :
                                            'id="xs-pipes-links-module-SharedModule-7e5ead7023bdf5de555fb55d65ef3fdbd6c51a5c84935935db8c2ae5fca0d747fa2d6f5a557e003500ba105f7212fe4a148ddb09a84cea60e162edee654d3993"' }>
                                            <li class="link">
                                                <a href="pipes/ShortNumberPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ShortNumberPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/API.html" data-type="entity-link" >API</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ApiService.html" data-type="entity-link" >ApiService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/HttpInterceptorService.html" data-type="entity-link" >HttpInterceptorService</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link" >AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/credentials.html" data-type="entity-link" >credentials</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Movie.html" data-type="entity-link" >Movie</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/token.html" data-type="entity-link" >token</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});