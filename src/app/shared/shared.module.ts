import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShortNumberPipe } from './pipes/short-number.pipe';
import { HighlightDirective } from './directives/highlight.directive';



@NgModule({
  declarations: [
    SearchBarComponent,
    HighlightDirective,
    ShortNumberPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    SearchBarComponent,
    HighlightDirective,
    ShortNumberPipe
  ]
})
export class SharedModule { }
